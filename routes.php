<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
//require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/SpeakerController.php';
require_once 'controllers/MeetupController.php';



// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/projets/mes%20projets/API2';

    SimpleRouter::group(['prefix' => $prefix], function () {
    //SimpleRouter::match(['get', 'post'], '/login', 'LoginController@login')->name('login');
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::get('/subscriber', 'SubscriberController@getSubscriber');/*->addMiddleware(APIMiddleware::class)*/
    SimpleRouter::get('/speaker', 'SpeakerController@getSpeaker');
    SimpleRouter::get('/affichMeetup', 'MeetupController@getMeetup');

    SimpleRouter::post('/meetup', 'MeetupController@addMeetup');


    

    SimpleRouter::get('/subscriber/{id}', 'SubscriberController@getWithId')->addMiddleware(APIMiddleware::class);


    //faire un router pour le DELETE
});

?>