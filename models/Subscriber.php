<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:43
 */
include_once 'config.php';
class Subscriber {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    public function getSubscriber() {
       
       $bdd = connect();
       $reponse = $bdd->query('SELECT * FROM subscriber');
       $subscriber = $reponse->fetchAll();
       return $subscriber;

    }
}
