<?php

require_once 'controllers/Controller.php';
require_once 'models/Speaker.php';

class SpeakerController extends Controller {
    function getSpeaker() {
        $speaker = new Speaker();
        echo json_encode($speaker->getSpeaker());
    }

    function getWithId($id) {
        echo json_encode('coucou'.$id);
    }
}