<?php

require_once 'controllers/Controller.php';
require_once 'models/Subscriber.php';

class SubscriberController extends Controller {
    function getSubscriber() {
        $subscriber = new Subscriber();
        echo json_encode($subscriber->getSubscriber());
    }

    function getWithId($id) {
        echo json_encode('coucou'.$id);
    }
}