<!DOCTYPE html>
<html lang="en">

<head>
   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <title>Document</title>
</head>

<body>
    <br>
    <br>
    <div class="header">
        <div class="logo">
            <img src="images/meetup.png" height="130px" width="30%"> </div>
        <div class="login">
            
            <div class="connexion"><a href="Connexion.php">Connexion</a></div>
        </div>
    </div>
    
    <div class="tableau">

        <div class="subscriber"><a href="#">subscriber</a>
            <?php include "formulaires/formSubscriber.php"; ?>
        </div>  <br>
             <script>
                 $('.subscriber').on('click',function(){
                     $('#register_form').toggle();
                 })

                 $('#return').on('click',function(){
                    $('#register_form').hide();
                 })
             </script>

        <div class="speaker">speaker<br>
            <form action="......." method="post">     
                <input type="text" class="speaker" />
                <button type="submit" class="addSpeaker">ajouter</button>       
        </div>


        <div class="meetup1"><h1>meetup</h1><br>
          <div id="add-meetup-form">
                    <input type="text" name="title" placeholder="titre"><br/>
                    <input type="date" name="date" placeholder="date"><br/>
                    <input type="text" name="location" placeholder="lieu"><br/>
                    <textarea name="description" placehoder="description"></textarea><br/>
                    <input type="button" value="ajouter" id="add-meetup-button"><br/>
                </div>
        </div>
    </div>
    
    <script type="text/javascript">

        // lier le bouton a la fonction addMeetup
       $('#add-meetup-button').on('click', addMeetup)

function addMeetup() {
        $.ajax({
            url : 'http://localhost/projets/mes%20projets/API2/meetup',
            type : 'POST',
            data: {
                title: $('[name=title]').val(),
                location: $('[name=location]').val(),
                date: $('[name=date]').val(),
                description: $('[name=description]').val(),
            }
        }).done(function (data) {
           data = JSON.parse(data);
           $('.meetup1').append(`<div class="meetup" data-id="${data.id}"><div>${data.title}</div></div>`)
        });

    }
    </script>
</body>

</html>